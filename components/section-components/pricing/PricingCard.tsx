import React, { useState } from "react";
import Image from "next/image";
// import { useAptabase } from '@aptabase/react';
import Button from "../../Button";
import { useRouter } from "next/router";
import { check, mostPopular } from "../../../assets";
import { CardObject } from "../../../helper/constant";
import { motion } from "framer-motion";

const PricingCard = ({
  description,
  price,
  services,
  title,
  isMobile,
  translateX,
  className,
  aos,
  setAppContext,
  pricing,
  index,
  exactPrice,
}: CardObject) => {
  const [bgPosition, setBgPosition] = useState({ x: 0, y: 0 });
  const [hoveredElement, setHoveredElement] = useState(null);
  const router = useRouter();

  const handleMouseMove = (e) => {
    const rect = e.currentTarget.getBoundingClientRect();
    const backgroundX = ((e.clientX - rect.left) / rect.width) * 100;
    const backgroundY = ((e.clientY - rect.top) / rect.height) * 100;
    setBgPosition({ x: backgroundX, y: backgroundY });
  };


  const handleMouseEnter = (e, element) => {
    setHoveredElement(element);
  };

  const resetHoveredElement = () => {
    setHoveredElement(null);
  };


  const handleNavigate = (href) => {
    router.push(href);
  };

  // const { trackEvent } = useAptabase();

  return (
    <motion.div
      data-aos={aos}
      style={{
        transform: `${isMobile
          ? `translateX(${translateX}px) ${translateX === 0 ? "scale(1.1)" : ""}`
          : ""
          } ${hoveredElement
            ? `skew(${(50 - bgPosition.x) * 0.1}deg, ${(50 - bgPosition.y) * 0.1}deg)`
            : "skew(0deg, 0deg)"
          }`,
        backgroundImage: hoveredElement
          ? `radial-gradient(circle at ${bgPosition.x}% ${bgPosition.y}%, #38D8CC, transparent 35%)`
          : `radial-gradient(circle at 50% 50%, #00a99d, transparent 0%)`,
        transition: "transform 0.3s ease, background-image 0.3s ease", // Smooth transition
      }}
      onMouseMove={handleMouseMove}
      onMouseEnter={(e) => handleMouseEnter(e, "card")}
      onMouseLeave={resetHoveredElement}
      className={`shadow-lg md:w-[25%] shadow-black py-5 px-3 xl:px-6 rounded-xl bg-main-lightBg hover:bg-black hover:shadow-main-teal ${className}`}
    >

      {index === 1 && (
        <Image
          className="absolute top-[-26.5%] left-[-4%] hidden lg:inline-block"
          src={mostPopular}
          alt="Most popular"
        />
      )}

      <div className="w-[80%] text-center text-[13px] sm:text-[0.9rem] xl:text-[1rem] font-miligrambold mx-auto py-[8px] text-black bg-main-teal rounded-lg -translate-y-8">
        {title}
      </div>

      <div className="py-2 lg:py-3 text-main-light_white text-center font-miligramLight text-[0.8rem] xl:text-[0.9rem] border-b-[1px] border-main-teal">
        <span className="text-[1.2rem] xl:text-[1.5rem] text-white font-miligramMedium">
          {price}&nbsp;
        </span>
        / hour
      </div>

      <Button
        OnClick={() => {
          // trackEvent('Pricing_Selected_Price', { Seleted_price: price });
          setAppContext(`${exactPrice}`);
          handleNavigate("start-project");
        }}
        text="Get Started"
        className="mx-auto text-[0.8rem] xl:text-[1rem] w-full block mt-3 sm:mt-5 bg-main-greenOpt-200 font-miligramLight text-main-lightTeal py-[6px] px-8 rounded-lg border-[1px] border-main-lightTeal hover:text-main-teal"
      />

      <ul className="mt-3 lg:mt-4 w-full xxl:w-[80%] mx-auto h-[150px] md:h-[170px] ">
        {services.map((item, i) => (
          <li
            key={i}
            className="text-white gap-2 flex text-[0.8rem] xl:text-[0.9rem] items-center mt-3 lg:mt-3 leading-none"
          >
            <Image src={check} alt="checked" className="h-3 w-3" />
            <span>{item}</span>
          </li>
        ))}
      </ul>
    </motion.div>
  );
};

export default PricingCard;
