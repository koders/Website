import React from "react";
import Image from "next/image";

interface Props {
  title: string;
  logo: string;
  description: string;
  rating: number;
  link: string;
}
const TestmonialCard = ({ title = "Anonymous", logo, description = "No description provided", rating = 0, link }: Props) => {
  return (
    <div
      onClick={() => {
        if (link) window.open(link, "_blank");
      }}
      className={`border-2 border-main-teal rounded-md p-5 bg-main-secondary mt-5 ${link ? "cursor-pointer" : ""
        } hover:shadow-lg transition-shadow duration-200`}
    >
      <div className="flex items-center mb-4">
        <Image
          src={logo || "/default-thumbnail.jpg"}
          alt={`${title}'s avatar`}
          width={50}
          height={50}
          className="rounded-full mr-4"
        />
        <div className="text-white text-[20px]">
          <span className="font-semibold">{title}</span>
          <div className="w-fit text-main-yellow mt-1">{Array(rating).fill("⭐").join(" ") || "No rating"}</div>
        </div>
      </div>
      <p className="text-main-light_white text-justify">{description}</p>
    </div>
  );
};

export default TestmonialCard;
