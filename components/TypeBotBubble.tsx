import React from "react";
import dynamic from "next/dynamic";

const Bubble = dynamic(() => import('@typebot.io/react').then((mod) => mod.Bubble), {
  ssr: false
});

const TypeBotBubble: React.FC = () => {
  return (
    <div className="bounce">
      <Bubble
        apiHost="https://forms.koders.in"
        typebot="discovery"
        theme={{ button: { backgroundColor: "#0d9488" } }}
      />
    </div>
  );
};

export default TypeBotBubble;
