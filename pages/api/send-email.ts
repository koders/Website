import { NextApiRequest, NextApiResponse } from 'next';
import nodemailer from 'nodemailer';

export default async function handler(req: NextApiRequest, res: NextApiResponse) {
  if (req.method === 'POST') {
    const { email, ticketToken, type } = req.body;

    const name = type === 'collaborate' ? req.body.name : req.body.fName;

    let subject: string;
    let text: string;

    if (type === 'start-project') {
      subject = 'Thank you for your project submission!';
      text = `Hello ${name},\n\nThank you for reaching out to us about your project. Your ticket number is: ${ticketToken}. Our team will contact you soon.\n\nBest Regards,\nTeam`;
    } else if (type === 'collaborate') {
      subject = 'Thank you for your interest in collaboration!';
      text = `Hello ${name},\n\nThank you for showing interest in collaborating with us. Your ticket number is: ${ticketToken}. Our team will reach out to you soon.\n\nBest Regards,\nTeam`;
    } else {
      res.status(400).json({ error: 'Invalid email type' });
      return;
    }

    try {
      const transporter = nodemailer.createTransport({
        service: 'Gmail',
        auth: {
          user: process.env.SMTP_USER,
          pass: process.env.SMTP_PASS,
        },
      });

      await transporter.sendMail({
        from: `"Project Team" <${process.env.SMTP_USER}>`,
        to: email,
        subject,
        text,
      });

      res.status(200).json({ success: true });
    } catch (error) {
      console.error("Error sending email:", error);
      res.status(500).json({ error: 'Error sending email' });
    }
  } else {
    res.status(405).json({ error: 'Method not allowed' });
  }
}
