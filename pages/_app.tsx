import "../styles/globals.css";
// import { AptabaseProvider } from '@aptabase/react';
import { AppContext } from "../components";
import TypebotBubble from "../components/TypeBotBubble";
import { useEffect, useState } from "react";
import axios from "axios";
import { useRouter } from "next/router";
import ReactGA from "react-ga4";

const GA_TRACKING_ID = process.env.NEXT_PUBLIC_GA_TRACKING_ID;


function MyApp({ Component, pageProps }) {
  const [jobs, setJobs] = useState<any>(null);
  const [appContext, setAppContext] = useState("");
  const router = useRouter();

  useEffect(() => {
    if (GA_TRACKING_ID) {
      ReactGA.initialize(GA_TRACKING_ID);
    }

    const handleRouteChange = (url: string) => {
      if (GA_TRACKING_ID) {
        ReactGA.send({ hitType: "pageview", page: url });
      }
    };


    router.events.on('routeChangeComplete', handleRouteChange);


    axios
      .get("/api", { headers: { home: true } })
      .then((data) => { })
      .catch((e) => { });


    return () => {
      router.events.off('routeChangeComplete', handleRouteChange);
    };
  }, [router.events]);

  return (
    <AppContext.Provider value={{ appContext, setAppContext, jobs, setJobs }}>
      {/* <AptabaseProvider appKey={process.env.NEXT_PUBLIC_APTAKEY}> */}
      <Component {...pageProps} />
      <TypebotBubble />
      {/* </AptabaseProvider> */}
    </AppContext.Provider>
  );
}

export default MyApp;
