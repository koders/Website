import Head from "next/head";
import { useRouter } from "next/router";
import React, { useEffect, useState } from "react";

import {
  Button,
  Divider,
  Footer,
  GradientText,
  Navbar,
  TestmonialCard,
} from "../components";
import axios from "axios";
import { FadeLoader } from "react-spinners";
import { shortReviewArray } from "../helper";
import { discordReviews, listOfBestReviewsUsers } from "../helper/constant";

const Testmonials = () => {
  const [loading, setLoading] = useState<boolean>(false);
  const [allReviews, setAllReviews] = useState<Array<any>>([[], [], []]);

  const router = useRouter();

  const handleNavigate = (href: string) => {
    router.push(href);
  };

  const fetchReview = async () => {
    try {
      setLoading(true);
      const res = await axios.get(`${process.env.NEXT_PUBLIC_STRAPI_URL}/api/reviews`, {
        headers: {
          Authorization: `Bearer ${process.env.NEXT_PUBLIC_STRAPI_TOKEN}`,
        },
      });
      const data = (res.data as { data: Array<any> }).data;

      if (data && data.length) {
        const reviews = data.map((review) => ({
          snippet: review.review_text || "No review text available",
          user: {
            name: review.user_name || "Anonymous",
            thumbnail: review.user_thumbnail || "/default-thumbnail.jpg",
            redirectTo: review.user_link || "#",
          },
          rating: review.rating || 0,
        }));

        setAllReviews(reviews);
      } else {

        setAllReviews(getFallbackReviews());
      }
    } catch (error) {
      console.error("Error fetching reviews:", error.response?.data || error.message);
      setAllReviews(getFallbackReviews());
    } finally {
      setLoading(false);
    }
  };

  const [currentIndex, setCurrentIndex] = useState(0);
  const [animate, setAnimate] = useState(false);

  useEffect(() => {
    const timer = setTimeout(() => {
      setAnimate(false);
    }, 500); // Match duration to your CSS animation
    return () => clearTimeout(timer);
  }, [currentIndex]);

  const handleNext = () => {
    if (currentIndex < allReviews.length - 1) {
      setAnimate(true);
      setCurrentIndex(currentIndex + 1);
    }
  };

  const handlePrev = () => {
    if (currentIndex > 0) {
      setAnimate(true);
      setCurrentIndex(currentIndex - 1);
    }
  };

  const getFallbackReviews = () => [
    {
      snippet: "Koders is amazing! Great team, awesome service.",
      user: {
        name: "John Doe",
        thumbnail: "/default-thumbnail.jpg",
        redirectTo: "#",
      },
      rating: 5,
    },
    {
      snippet: "Highly recommend! Very professional and responsive.",
      user: {
        name: "Jane Smith",
        thumbnail: "/default-thumbnail.jpg",
        redirectTo: "#",
      },
      rating: 4,
    },
    {
      snippet: "The best experience ever! Will work with them again.",
      user: {
        name: "Sam Lee",
        thumbnail: "/default-thumbnail.jpg",
        redirectTo: "#",
      },
      rating: 5,
    },
  ];



  useEffect(() => {
    setTimeout(() => {
      window.scroll({
        top: 0,
        left: 0,
        behavior: "smooth",
      });
    });
  }, []);

  useEffect(() => {
    if (!allReviews[0].length || !allReviews[1].length || !allReviews[2].length) {
      fetchReview();
    }
  }, []);

  useEffect(() => {
    const tryAgain = document.getElementById("tryAgain");
    tryAgain?.addEventListener("click", () => {
      fetchReview();
    });
  });

  return (
    <div className="bg-main-primary overflow-hidden relative">
      <Head>
        <title>Reviews</title>
      </Head>
      <Navbar />
      <div className="pb-10 pt-28">
        <GradientText
          className="text-[2.2rem] leading-none mb-3 md:mb-0 font-miligrambold  sm:text-[2.5rem] md:text-[2.8rem] mt-10 w-[90%] mx-auto text-center bg-gradient-to-r from-white to-main-teal"
          text="Don't just take our word for it."
        />
        <p className="mt-2 text-[0.8rem] sm:text-[1.3rem] w-[80%] sm:w-1/2 mx-auto text-center text-main-light_white pb-2 font-miligramText400 leading-none">
          Take a look at what some of our customers have to say about Koders.
        </p>
        <Divider className="mt-4 md:mt-14" />
        {loading ? (
          <FadeLoader
            className="w-fit block mx-auto"
            loading={loading}
            color="#00a99d"
          />
        ) : (
          <div className="relative flex justify-center items-center">
            <button
              className="absolute left-0 z-10 p-2"
              onClick={handlePrev}
              disabled={currentIndex === 0}
            >
              Prev
            </button>
            <div className="flex gap-3 w-[85%] mx-auto flex-wrap overflow-hidden">
              {allReviews.map((item, i) => (
                <div
                  key={i}
                  className={`transition-transform duration-500 ${animate && currentIndex === i
                    ? 'transform scale-105 translate-y-2'
                    : ''
                    }`}
                >
                  {item.user && (
                    <TestmonialCard
                      description={item.snippet || "No snippet available"}
                      logo={item.user?.thumbnail}
                      title={item.user?.name}
                      rating={item.rating || 0}
                      link={item.user?.redirectTo || "#"}
                    />
                  )}
                </div>
              ))}
            </div>
            <button
              className="absolute right-0 z-10 p-2"
              onClick={handleNext}
              disabled={currentIndex === allReviews.length - 1}
            >
              Next
            </button>
          </div>
        )}
        <Button
          OnClick={() => handleNavigate('start-project')}
          text="Get Started"
          className="text-[0.8rem] xxl:text-[1rem] mx-auto block mt-8 sm:mt-12 bg-main-greenOpt-200 font-miligramMedium text-main-lightTeal py-[8px] sm:py-[10px] px-6 sm:px-9 rounded-lg border-[1px] border-main-lightTeal hover:bg-main-lightTeal hover:text-white"
        />
        <Divider className="mt-16 py-2" />
      </div>
      <Footer />
    </div>
  );
};

export default Testmonials;
