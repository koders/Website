import Head from "next/head";
import { FormikHelpers } from "formik";
import React, { useContext, useState } from "react";
import { v4 as uuidv4 } from 'uuid';
import axios from "axios";
import "aos/dist/aos.css";

import {
  AppContext,
  ButtonsGroup,
  Divider,
  Faq,
  Footer,
  Form,
  GradientText,
  Navbar,
} from "../components";
import { sendClientDetails } from "../helper/webhook";
import { faq, typeOfProjects } from "../helper/constant";
import { text } from "stream/consumers";
// import { useSetDataOnServer } from "../helper/careerHooks";

export interface FormState {
  technologies: Array<string>;
}

interface initialState {
  aboutProject: string;
  budget: string;
  timeline: string;
  email: string;
  mobile: string;
  fName: string;
  lName: string;
  company: string;
  role: string;
  hearAboutUs: string;
  pricingPlan: string;
}

interface Message {
  text: string;
  type: "success" | "error";
}

const StartProject = () => {
  const [showLoader, setShowLoader] = useState(false);
  const [technologies, setTechnologies] = useState<Array<string>>([
    "Web Development",
    "UI/UX",
  ]);
  const [isExpand, setIsExpand] = useState("");
  // const sendData = useSetDataOnServer();
  const { appContext, setAppContext }: any = useContext(AppContext);
  const [message, setMessage] = useState<Message | null>(null);



  const handleExpand = (question: string) => {
    if (isExpand === question) setIsExpand("");
    else setIsExpand(question);
  };

  const handleSubmitForm = async (
    value: initialState,
    helper: FormikHelpers<initialState>
  ) => {
    try {
      setShowLoader(true);
      setMessage(null);
      const ticketToken = uuidv4();
      const res = await sendClientDetails({ ...value, technologies, ticketToken });

      if (res) {
        await fetch('/api/sendEmail', {
          method: 'POST',
          headers: {
            'Content-Type': 'application/json',
          },
          body: JSON.stringify({
            email: value.email,
            fName: value.fName,
            ticketToken,
          }),
        });

        helper.resetForm();
        setMessage({
          text: "Your response has been recorded, and a thank-you email has been sent.",
          type: "success",
        });
      } else {
        setMessage({
          text: "Unable to record your response. Please try again later.",
          type: "error",
        });
      }
      console.log("Message set to:", message);
      setShowLoader(false);
    } catch (error) {
      setMessage({
        text: "An error occurred while submitting your form. Please try again later.",
        type: "error",
      });
      console.error("Error in form submission or email:", error);
    } finally {
      setShowLoader(false);
    }
  };

  const handleClick = (item: string) => {
    let temp: Array<any> = [];
    if (technologies?.find((elm) => elm === item)) {
      temp = technologies.filter((elm) => elm !== item);
    } else {
      temp = [...technologies, item];
    }
    setTechnologies(temp);
    if (temp.length === 0) {
      setTechnologies(["Other"]);
    }
  };

  return (
    <div className="bg-main-primary overflow-hidden relative">
      <Head>
        <title>Start Project</title>
      </Head>
      <Navbar />
      <div
        data-aos="fade-up"
        className="py-20 sm:py-28 w-[90%] md:w-[85%] mx-auto aos-animate"
      >
        <GradientText
          className="text-[2.2rem] leading-none mb-3 md:mb-0 md:leading-normal  sm:text-[2.8rem] w-fit mx-auto text-center mt-9 bg-gradient-to-r from-white to-main-teal font-miligrambold"
          text="Start your project"
        />
        <Divider className="mt-16 md:py-2" />
        <ButtonsGroup
          {...{
            technologies,
            handleClick,
            buttonsArray: [...typeOfProjects],
          }}
        />
        <Divider className="mt-16" />
        <Form
          {...{
            handleSubmitForm,
            showLoader,
            setShowLoader,
            appContext,
            setAppContext,
          }}
        />
        {message && (
          <p
            className={`text-center mt-4 ${message.type === "success" ? "text-green-600" : "text-red-600"
              }`}
          >
            {message.text}
          </p>
        )}
        <Divider className="mt-20" />
        <GradientText
          aos="fade-up"
          className="text-[2.2rem] leading-none mb-3 md:mb-0 md:leading-normal  sm:text-[2.8rem]  w-fit mx-auto text-center bg-gradient-to-r from-white to-main-teal font-miligrambold aos-animate"
          text="Frequently Asked Questions"
        />
        <Divider className="mt-8 xxl:mt-16 xxl:pt-3 aos-animate" />
        {faq.map((item, i) => (
          <Faq
            answer={item.answer}
            question={item.question}
            onClick={handleExpand}
            show={item.question === isExpand}
            key={i}
          />
        ))}
      </div>
      <Footer />
    </div>
  );
};

export default StartProject;
