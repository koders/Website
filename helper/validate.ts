import * as yup from "yup";

const urlRegex = /^(ftp|http|https):\/\/[^ "]+$/;
const emailRegex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
const nameRegex = /^[A-Za-z\s]+$/;

export const jobValidationSchema = yup.object({
  fName: yup
    .string()
    .matches(nameRegex, "Name should only contain alphabates.")
    .required("First Name is a mandatory field."),
  lName: yup
    .string()
    .matches(nameRegex, "Name should only contain alphabates.")
    .required("Last Name is a mandatory field."),
  email: yup
    .string()
    .matches(emailRegex, "Provided email address is invalid.")
    .required("Email Address is a mandatory field."),
  mobile: yup.string().required("Phone Number is a mandatory field."),
  joiningIn: yup.string().required("This is a mandatory field."),
  linkedIn: yup
    .string()
    .optional()
    .matches(urlRegex, "LinkedIn URL is invalid."),
  portfolioURL: yup
    .string()
    .optional()
    .matches(urlRegex, "Portfolio URL is invalid."),
  hiringReason: yup.string().required("This is a mandatory field."),
  hearAboutUs: yup.string().required("This is a mandatory field."),
});

export const projectDataSchema = yup.object({
  aboutProject: yup.string().required("This is a mandatory field."),
  budget: yup.string().required("Estimated Budget is a mandatory field."),
  timeline: yup.string().required("Estimated Timeline is a mandatory field."),
  email: yup
    .string()
    .matches(emailRegex, "Provided email address is invalid.")
    .required("Email Address is a mandatory field."),
  mobile: yup.string().required("Phone Number is a mandatory field."),
  fName: yup
    .string()
    .matches(nameRegex, "Name should only contain letters.")
    .required("First Name is a mandatory field."),
  lName: yup
    .string()
    .matches(nameRegex, "Name should only contain letters.")
    .required("Last Name is a mandatory field."),
  company: yup.string().notRequired(),
  role: yup.string().notRequired(),
  hearAboutUs: yup.string().required("This is a mandatory field."),
  pricingPlan: yup.string().required("This is a mandatory field."),
});

export const collabrationPageSchima = yup.object({
  name: yup
    .string()
    .min(2)
    .matches(nameRegex, "Name should only contain alphabates.")
    .required("First Name is a mandatory field."),
  email: yup
    .string()
    .matches(emailRegex, "Provided email address is invalid.")
    .required("Email Address is a mandatory field."),
  mobile: yup.string().required("Phone Number is a mandatory field."),
  company: yup.string().required("Company Name is a mandatory field."),
  companySize: yup.string().required("Company Size is a mandatory field."),
  location: yup.string().required("Company Location is a mandatory field."),
  websiteURL: yup
    .string()
    .matches(urlRegex, "Website/LinkedIn URL is invalid.")
    .required("Website/LinkedIn URL is a mandatory field."),
  jobTitle: yup.string().required("Job Title is a mandatory field."),
  questionOne: yup.string().required("This is a mandatory field."),
  questionTwo: yup.string().required("This is a mandatory field."),
  hearAboutUS: yup.string().required("This is a mandatory field."),
});

export const contactValidationSchima = yup.object({
  name: yup
    .string()
    .min(2)
    .matches(nameRegex, "Name should only contain alphabates.")
    .required("Name is a mandatory field."),
  email: yup
    .string()
    .matches(emailRegex, "Email Address is invalid.")
    .required("Email Address is a mandatory field."),
  message: yup.string().required("Message is a mandatory field."),
});

export const emailValidation = yup.object({
  email: yup
    .string()
    .matches(emailRegex, "Provided email address is invalid.")
    .required("Provided email address is mandatory."),
});

export type JobValidationSchema = yup.InferType<typeof jobValidationSchema>;
export type ProjectDataSchema = yup.InferType<typeof projectDataSchema>;
export type CollaborationPageSchema = yup.InferType<
  typeof collabrationPageSchima
>;
export type ContactValidationSchema = yup.InferType<
  typeof contactValidationSchima
>;
export type EmailValidationSchema = yup.InferType<typeof emailValidation>;
